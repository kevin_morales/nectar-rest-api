package db

import "errors"

var DuplicateKeyError = errors.New("duplicate key error")
